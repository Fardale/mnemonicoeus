(** Example of memory leak. This example can be used to illustrate how to look for
a memory leak using this tools. *)

open Containers

type log = { n : int; message : string }

let log_buffer = Vector.create ()

let push_log n message =
  CCVector.push log_buffer { n; message }

let () =
  CCIO.read_lines_seq stdin
  |> Seq.iteri (fun i s ->
         if i mod 100 = 0 then Dump.full (Printf.sprintf "leak_%i.dump" (i/100));
         push_log i s)
