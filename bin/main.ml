open Containers
open Mnemonicoeus

module Histogram_key = struct
  (** Module defining the keys used to build the histogram of value present in memory. *)

  type t =
    | Hash of int
    | String
    | Closure
    | Object
    | Lazy
    | Custom
    | Double
    | Forward

  let equal a1 a2 =
    match (a1, a2) with
    | Hash h1, Hash h2 -> Int.equal h1 h2
    | String, String
    | Closure, Closure
    | Object, Object
    | Lazy, Lazy
    | Custom, Custom
    | Double, Double
    | Forward, Forward ->
        true
    | _ -> false

  let hash = Hash.poly

  let pp fmt = function
    | Hash n -> Format.fprintf fmt "%i" n
    | String -> Format.fprintf fmt "String"
    | Closure -> Format.fprintf fmt "Closure"
    | Object -> Format.fprintf fmt "Object"
    | Lazy -> Format.fprintf fmt "Lazy"
    | Custom -> Format.fprintf fmt "Custom"
    | Double -> Format.fprintf fmt "Double"
    | Forward -> Format.fprintf fmt "Forward"
end

module Histogram = CCHashtbl.Make (Histogram_key)
(** The histogram maps key to the number of time this type of value have been seen in
    memory
*)

let main filename =
  let histogram = Histogram.create 10 in
  CCIO.with_in ~flags:[ Open_binary; Open_rdonly ] filename (fun ic ->
      let file_descr = Unix.descr_of_in_channel ic in
      let memory = Memory.load file_descr in
      CCList.iter
        (fun root ->
          let seen = ref Memory.Int64Set.empty in
          Memory.scan
            (fun chunk addr ->
              let header = Memory.get_header chunk addr in
              let approx =
                if header.profinfo <> 0 then Histogram_key.Hash header.profinfo
                else
                  match header.tag with
                  | Header.Tag.Closure_tag -> Histogram_key.Closure
                  | Header.Tag.String_tag -> Histogram_key.String
                  | Header.Tag.Object_tag -> Histogram_key.Object
                  | Header.Tag.Lazy_tag -> Histogram_key.Lazy
                  | Header.Tag.Custom_tag -> Histogram_key.Custom
                  | Header.Tag.Double_tag -> Histogram_key.Double
                  | Header.Tag.Forward_tag -> Histogram_key.Forward
                  | Header.Tag.Scan_Tag _ -> Histogram_key.Hash 0
                  | t ->
                      Format.eprintf "Other tag: %a@." Header.Tag.pp t;
                      Histogram_key.Hash 0
              in
              Histogram.update histogram
                ~f:(fun _ value ->
                  match value with None -> Some 1 | Some v -> Some (v + 1))
                ~k:approx)
            seen memory root)
        memory.roots);
  Histogram.to_list histogram
  |> List.sort Stdlib.compare
  |> Format.printf "@[<v>%a@]@."
       (List.pp (fun fmt (x, y) -> Format.fprintf fmt "%a -> %i" Histogram_key.pp x y))

(* Cmdliner *)
open Cmdliner

let cmd =
  let dump_file =
    let doc = "File containing the dump to analyse" in
    Arg.(required & pos 0 (some file) None & info [] ~docv:"DUMP" ~doc)
  in
  let doc = "Analyse memory dump" in
  let man =
    [
      `S Manpage.s_bugs;
      `P "Email bug reports to <fardale+mnemoicoeus at crans.org>";
    ]
  in
  let sdocs = Manpage.s_common_options in
  let exits = Cmd.Exit.defaults in
  let info = Cmd.info "Mnemonicoeus" ~version:"0.0.0" ~doc ~sdocs ~exits ~man in
  Cmd.v info Term.(const main $ dump_file)

let () = exit @@ Cmd.eval cmd
