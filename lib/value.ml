open Containers

(*type t =
  | Immediate of int
  | Block of block
  | Closure
  | String of string
  | Float of float
  | FloatArray of floatarray
  | Abstract
  | Custom
  | Pointer of int

and block = { header : Header.t; fields : t array }*)

let word_size = 8

let whsize hd =
  let { Header.wosize; _ } = Header.build hd in
  wosize + 1

let is_block x = Int64.(x land 1L = 0L)
