(** Module abstracting what an ocaml value is *)

(* type t =
  | Immediate of int
  | Block of block
  | Closure
  | String of string
  | Float of float
  | FloatArray of floatarray
  | Abstract
  | Custom
  | Pointer of int

and block = { header : Header.t; fields : t array} *)

val word_size : int
(** Number of byte in a word. Only 64bit plateform are supported, therefore [word_size = 8].*)

val whsize : int64 -> int
(** [whsize v] returns the size of the ocaml value [v] including the header. *)

val is_block : int64 -> bool
(** [is_block v] returns true if the value is a block and not an immediate value *)
