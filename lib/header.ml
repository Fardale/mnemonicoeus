open Containers

module Profinfo = struct
  type t = int

  let width = 26
  let shift = 64 - width
  let mask = Int64.((1L lsl width) - 1L)
  let get hd = Int64.((hd lsr shift) land mask |> to_int)
end

module Tag = struct
  type t =
    | Scan_Tag of int
    | Closure_tag
    | String_tag
    | Double_tag
    | Double_array_tag
    | Abstract_tag
    | Custom_tag
    | Object_tag
    | Forward_tag
    | Infix_tag
    | Lazy_tag

  let scan = function
    | Scan_Tag _ | Lazy_tag | Closure_tag | Object_tag | Infix_tag | Forward_tag
      ->
        true
    | _ -> false
  (* TODO: Forward_tag should be scan but I don't understand the code *)

  let mask = Int64.((1L lsl 8) - 1L)

  let get hd : t =
    match Int64.(hd land mask |> to_int) with
    | 255 -> Custom_tag
    | 254 -> Double_array_tag
    | 253 -> Double_tag
    | 252 -> String_tag
    | 251 -> Abstract_tag
    | 250 -> Forward_tag
    | 249 -> Infix_tag
    | 248 -> Object_tag
    | 247 -> Closure_tag
    | 246 -> Lazy_tag
    | n ->
        assert (n >= 0);
        assert (n < 251);
        Scan_Tag n

  let pp fmt t =
    Format.fprintf fmt "%s"
      (match t with
      | Scan_Tag n -> "Scan_Tag " ^ Int.to_string n
      | Closure_tag -> "Closure_tag"
      | String_tag -> "String_tag"
      | Double_tag -> "Double_tag"
      | Double_array_tag -> "Double_array_tag"
      | Abstract_tag -> "Abstract_tag"
      | Custom_tag -> "Custom_tag"
      | Object_tag -> "Object_tag"
      | Forward_tag -> "Forward_tag"
      | Infix_tag -> "Infix_tag"
      | Lazy_tag -> "Lazy_tag")
end

module Wosize = struct
  type t = int

  let mask = Int64.((1L lsl Int.(54 - Profinfo.width)) - 1L)
  let get hd : t = Int64.((hd lsr 10) land mask |> to_int)
end

module Color = struct
  (* Helpers for the colors *)

  type t = White | Gray | Blue | Black

  open Int64

  let caml_white = 0L lsl 8
  let caml_gray = 1L lsl 8
  let caml_blue = 2L lsl 8
  let caml_black = 3L lsl 8

  let get hd =
    let caml_color = hd land caml_black in
    if caml_color <= caml_gray then
      if caml_color = caml_white then White else Gray
    else if caml_color = caml_blue then Blue
    else Black

  let darken hd = hd lor caml_black

  let pp fmt color =
    Format.fprintf fmt "%s"
      (match color with
      | White -> "White"
      | Gray -> "Gray"
      | Blue -> "Blue"
      | Black -> "Black")
end

type t = {
  profinfo : int; (* Hash from let-def fork *)
  wosize : int; (* Size of the block in number of word *)
  color : Color.t;
  tag : Tag.t;
}

let build hd =
  {
    profinfo = Profinfo.get hd;
    wosize = Wosize.get hd;
    color = Color.get hd;
    tag = Tag.get hd;
  }

let pp fmt hd =
  Format.fprintf fmt "|%i|%i|%a|%a|" hd.profinfo hd.wosize Color.pp hd.color
    Tag.pp hd.tag
