(** Abstraction of a memory dump. *)

(** A dump is loaded by mapping the file into the memory using {!Unix.map_file}

    Every chunk of memory allocated by the GC is aligned on words. It does not contains
    value smaller than words and therefore could be address directly by words (using
    [int64]). But the data segment, that contains C values, is not. Therefore, we load
    the memory using [int8]. To facilitate the read of ocaml value, custom index operator
    are defined.
*)

type block =
  (int, Bigarray.int8_unsigned_elt, Bigarray.c_layout) Bigarray.Array1.t
(** Portion of memory *)

type chunk = {
  start : Int64.t;  (** address of the memory portion *)
  size : int;  (** Size in byte *)
  block : block;  (** Actual memory *)
}
(** A chunk represent a continuous portion of memory. *)

type t = { roots : Int64.t list; chunks : chunk array }
(** The memory of an ocaml program consists of a set of chunks and the roots of the programs.
    The first chunk contains the minor heap and the last chunk contains the data segment. *)

val get_value : block -> int -> int64
(** [get_value b i] returns the [int64] value that start at position [i]. *)

val set_value : block -> int -> int64 -> unit
(** [set_value b i v] write the value [v:int64] starting at position [i]. *)

val ( .%{} ) : block -> int -> int64
(** Index operator corresponding to {!get_value} *)

val ( .%{}<- ) : block -> int -> int64 -> unit
(** Index operator corresponding to {!set_value} *)

val load : Unix.file_descr -> t
(** Load a dump.
    This function does not load data in memory but map part of the file pointed by the
    file description to memory. The file containing the dump should be kept open as long
    as the memory is used.
*)

val get_chunk : t -> int64 -> chunk option
(** [get_chunk m addr] returns [Some chunk] if the address [addr] point to something in
    [chunk] and [None] otherwise.
*)

val get_chunk_exn : t -> int64 -> chunk
(** Unsafe version of {!get_chunk}.
    @raise Failure *)

val get_header : chunk -> int64 -> Header.t
(** [get_header c v] return the parsed header of the value [v] in [c].
    [value] must be a pointer to a block in [c].
    @raise Invalid_argument
*)

val pp : Format.formatter -> t -> unit
(** Pretty printer *)

module Int64Set : CCSet.S with type elt = int64
(** Set of [int64] *)

val scan : (chunk -> int64 -> unit) -> Int64Set.t ref -> t -> int64 -> unit
(** [scan f seen m r] scans the memory [m] starting at the root [r] and pass every
    reachable block to the function [f]. [f] is not apply on immediate value. [seen]
    is used to store seen address to not scan a value twice.
*)
