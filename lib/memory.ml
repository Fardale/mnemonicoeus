open Containers
open Bigarray

type block = (int, int8_unsigned_elt, c_layout) Array1.t
type chunk = { start : Int64.t; size : int; (* Size in byte *) block : block }

type t = {
  roots : Int64.t list;
  chunks : chunk array (* The first chunk reprensent the minor heap *);
}

let get_value file index =
  let res = ref 0L in
  for i = 0 to Value.word_size - 1 do
    res := Int64.(!res lor (of_int file.{Int.(index + i)} lsl Int.(i * 8)))
  done;
  !res

let set_value file index value =
  for i = 0 to Value.word_size - 1 do
    file.{index + i} <- Int64.((value lsr Int.(i * 8)) |> to_int)
  done

let ( .%{} ) = get_value
let ( .%{}<- ) = set_value

(** Read all the root from the file until a value 0 indicating the end. Start at position start *)
let load_roots file start =
  let rec aux acc i =
    let root = file.%{i} in
    if Int64.(root = 0L) then List.rev acc
    else aux (root :: acc) (i + Value.word_size)
  in
  aux [] start

(** Read chunk of memory, could be a chunk from the major_heap, the minor heap or the data segment *)
let load_chunks file =
  let make i =
    let start = file.%{i} in
    let size = Int64.to_int file.%{i + Value.word_size} in
    assert (size > 0);
    let block = Array1.sub file (i + (2 * Value.word_size)) size in
    { start; size; block }
  in
  let rec aux acc i =
    let chunk = make i in
    if i + chunk.size + (2 * Value.word_size) < Array1.size_in_bytes file then
      aux (chunk :: acc) (i + chunk.size + (2 * Value.word_size))
    else (
      assert (i + chunk.size + (2 * Value.word_size) = Array1.size_in_bytes file);
      List.rev (chunk :: acc))
  in
  aux []

(** Load a dump given the filename *)
let load file_descr =
  let file =
    array1_of_genarray
    @@ Unix.map_file file_descr Int8_unsigned c_layout false [| -1 |]
  in
  let roots = load_roots file 0 in
  let chunks =
    load_chunks file ((List.length roots + 1) * Value.word_size)
    |> Array.of_list
  in
  { roots; chunks }

let get_chunk memory address =
  Array.find_opt
    (fun c -> Int64.(c.start <= address && address - c.start <= of_int c.size))
    memory.chunks

let get_chunk_exn memory address =
  match get_chunk memory address with
  | Some c -> c
  | None -> failwith "Memory.get_chunk_exn"

let get_pos chunk address = Int64.(address - chunk.start |> to_int)

let get_header_raw chunk address =
  if Int64.(address < chunk.start || address - chunk.start > of_int chunk.size)
  then invalid_arg "Memory.get_header_raw";
  chunk.block.%{get_pos chunk address - Value.word_size}
(* The address point to the first field and not to the header therefore the - Value.word_size *)

let get_header chunk address = Header.build (get_header_raw chunk address)

let set_header_raw chunk address hd =
  if Int64.(address < chunk.start || address - chunk.start > of_int chunk.size)
  then invalid_arg "Memory.set_header_raw";
  chunk.block.%{get_pos chunk address - Value.word_size} <- hd
(* The address point to the first field and not to the header therefore the - Value.word_size *)

let get_field chunk address n =
  (* The address point to the first field and not to the header *)
  let pos = get_pos chunk address in
  let header = get_header chunk address in
  if n < 0 || n >= header.wosize then invalid_arg "Memory.field"
  else if pos + (n * Value.word_size) >= chunk.size then
    invalid_arg (Printf.sprintf "Memory.field %i" n)
  else chunk.block.%{pos + (n * Value.word_size)}

let pp fmt memory =
  Format.fprintf fmt "Roots:@ @[%a@]@.@." (CCList.pp CCInt64.pp) memory.roots;
  Format.fprintf fmt "Chunks:@ @[%a@]@."
    (CCArray.pp (CCPair.pp CCInt64.pp Int.pp))
    (Array.map (fun c -> (c.start, c.size)) memory.chunks)

module Int64Set = Set.Make (Int64)

let scan f seen memory root =
  let rec aux addr =
    if Value.is_block addr then
      match get_chunk memory addr with
      | None -> Format.eprintf "Address not in memory %a@." Int64.pp addr
      | Some chunk ->
          let addr, header =
            match get_header chunk addr with
            | { tag = Header.Tag.Infix_tag; wosize; _ } ->
                Format.eprintf "Infix_tag scan@.";
                let addr =
                  Int64.(addr - of_int (Int.mul wosize Value.word_size))
                in
                (addr, get_header chunk addr)
            | header -> (addr, header)
          in
          if not (Int64Set.mem addr !seen) then (
            seen := Int64Set.add addr !seen;
            f chunk addr;
            let offset =
              match header.tag with
              | Header.Tag.Closure_tag ->
                  let closinfo = get_field chunk addr 1 in
                  Int64.((closinfo lsl 8) lsr 9 |> to_int)
              | _ -> 0
            in
            if Header.Tag.scan header.tag then
              if chunk.size < header.wosize * Value.word_size then
                Format.eprintf "I made a mistake, please correct me: %a %a@."
                  Int64.pp addr Header.pp header
              else
                for i = offset to header.wosize - 1 do
                  try
                    let field = get_field chunk addr i in
                    if Value.is_block field then aux field else ()
                  with Invalid_argument _ ->
                    Format.eprintf "Field not in memory: %a@." Int64.pp addr
                done)
  in
  aux root

(* Not used *)
let _mark memory =
  let seen = ref Int64Set.empty in
  List.iter
    (fun root ->
      scan
        (fun chunk addr ->
          let hd = get_header_raw chunk addr in
          (* if Int64.(hd <> Header.Color.darken hd) then
               Printf.printf "Need to darken\n"
             else Printf.printf "Already dark\n";*)
          set_header_raw chunk addr (Header.Color.darken hd))
        seen memory root)
    memory.roots
