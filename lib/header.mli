(** Module defining the type of header and function to parse them *)

(** Structure of the header:
    (see {{: https://github.com/FardaleM/ocaml/blob/60a8be0dca527155816d7420c17216da36767db7/runtime/caml/mlvalues.h#L91-L114} [runtime/caml/mlvalues.h]})

For 16-bit and 32-bit architectures:
{v
     +--------+-------+-----+
     | wosize | color | tag |
     +--------+-------+-----+
bits  31    10 9     8 7   0
v}

For 64-bit architectures:
{v
     +--------+-------+-----+
     | wosize | color | tag |
     +--------+-------+-----+
bits  63    10 9     8 7   0
v}

For x86-64 with profinfo:
  [P = PROFINFO_WIDTH] (as set by "configure", currently 26 bits, giving a
   maximum block size of just under 4Gb)
{v
     +----------------+----------------+-------------+
     | profiling info | wosize         | color | tag |
     +----------------+----------------+-------------+
bits  63        (64-P) (63-P)        10 9     8 7   0
v}
*)

module Profinfo : sig
  (** Module for the profiling information part of the header *)

  type t
  (** Type of the profiling information *)

  val width : int
  (** Size of the profinfo section in the header *)

  val get : int64 -> t
  (** Get the profiling information from a header *)
end

module Tag : sig
  (** Module for tags *)

  (** Possible tags of an ocaml value (see {{: https://github.com/FardaleM/ocaml/blob/tagl-414/runtime/caml/mlvalues.h} [runtime/caml/mlvalues.h]}) *)
  type t =
    | Scan_Tag of int
    | Closure_tag
    | String_tag
    | Double_tag
    | Double_array_tag
    | Abstract_tag
    | Custom_tag
    | Object_tag
    | Forward_tag
    | Infix_tag
    | Lazy_tag

  val scan : t -> bool
  (** [scan t] returns true if the fields of a value with the tag [t] should
   * be scan by the GC. *)

  val get : int64 -> t
  (** Get the tag from a header *)

  val pp : Format.formatter -> t -> unit
  (** Pretty printing *)
end

module Color : sig
  (** Module for the color in a header *)

  (** There is four different possible colors
   * (see {{: https://github.com/FardaleM/ocaml/blob/60a8be0dca527155816d7420c17216da36767db7/runtime/caml/gc.h#L22-L25} [runtime/caml/gc.h]} *)
  type t = White | Gray | Blue | Black

  val get : int64 -> t
  (** Get the color from a header *)

  val darken : int64 -> int64
  (** [darken h] returns the header [h] with the color {!Black} *)

  val pp : Format.formatter -> t -> unit
  (** Pretty printing *)
end

type t = {
  profinfo : int;  (** Hash from let-def fork *)
  wosize : int;  (** Size of the block in number of word *)
  color : Color.t;
  tag : Tag.t;
}

val build : int64 -> t
(** Build the header from it memory reprensentation as an [int64] *)

val pp : Format.formatter -> t -> unit
(** Pretty printing *)
